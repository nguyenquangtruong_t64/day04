

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>LAB 04</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>   
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>   
</head>
<body>
<?php
    $nameErr = $genderErr = $majorErr = $dateErr = "";
    $name = $gender = $major = $date = $address = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
        $nameErr = "Hãy nhập tên.";
        } else {
        $name = test_input($_POST["name"]);
        }
        if (empty($_POST["gender"])) {
            $genderErr = "Hãy chọn giới tính.";
        } else {
            $gender = test_input($_POST["gender"]);
        }
        if (strlen($_POST["major"])===0) {
            $majorErr = "Hãy chọn phân khoa.";
        } else {
            $major = test_input($_POST["major"]);
        }
        if (empty($_POST["date"])) {
            $dateErr = "Hãy nhập ngày sinh.";
        }else {
            if(!validateDate($_POST["date"],$format = 'dd-mm-yyyy')){
            $dateErr = "Hãy nhập ngày sinh đúng định dạng.";
            }else {
            $date = test_input($_POST["date"]);
            }
        }
    }

    function validateDate($date, $format = 'dd-mm-yyyy') {
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>
<div class="border" style = "border: 2px solid rgb(26, 117, 255);
        background-color: white;
        padding: 20px 30px 20px 30px;
        position: absolute;
        margin-left: 25%;
        margin-top: 5%;
        width: 50%;
        ">
        <span class="error" style="color: red;"> <?php echo $nameErr;?></span><br> <br>
        <span class="error" style="color: red;"> <?php echo $genderErr;?></span> <br> <br>
        <span class="error" style="color: red;"> <?php echo $majorErr;?></span> <br> <br>
        <span class="error" style="color: red;"> <?php echo $dateErr;?></span> <br> <br>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    
            <div style=" margin-left: 10%;" class="form">
                <div class="name">
                    <span style="background-color:rgb(41, 218, 47);
                    width: 120px;
                    display: inline-block;
                    padding : 10px 10px;
                    color :white;
                    border:2px solid rgb(26, 117, 255)" for="name">Họ và tên <span style="color:red;">*</span></span>
                    <input style=" margin-left:6%; border: 2px solid rgb(52, 37, 255); height: 40px; width: 370px"
                    type="text" name="name" >
                </div>
                <div style="margin-top:20px">
                    <span style="background-color:rgb(41, 218, 47);
                    width: 120px;
                    display: inline-block;
                    padding : 10px 10px;
                    color :white;
                    border:2px solid rgb(26, 117, 255)" for="name">Giới tính <span style="color:red;">*</span></span> 
                    <?php
                    $gender = array("0"=>"Nam","1"=>"Nữ");
                    for ($i = 0; $i <= 1; $i++) {
                        if($i%2 == 0) {
                        echo '<input value="'.$gender[$i].'" id="'.$gender[$i].'" style="margin-left:45px" type="radio" name="gender">'.$gender[$i];
                        echo '<input value="'.$gender[$i+1].'" id="'.$gender[$i+1].'" style="margin-left:30px" type="radio" name="gender">'.$gender[$i+1];
                        }
                    }
                    ?>            
                </div>
                <div class = "subject" style="margin-top:20px">
                    <span class="subject_selection" 
                    style="background-color:rgb(41, 218, 47);
                    width: 120px;
                    display: inline-block;
                    padding : 10px 10px;
                    color :white;
                    border:2px solid rgb(26, 117, 255)">Phân khoa <span style="color:red;">*</span></span>
                    <select name="major" id="subjects" style=" margin-left: 6%; width: 150px; height: 40px; border:2px solid rgb(26, 117, 255);">
                    <?php
                        $subject = array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                        foreach($subject as $key => $val){
                            $selected = ($key == ' ') ? 'selected="selected"' : '';
                            echo '<option  value="'. $val .'" ' . $selected . ' >'. $val .'</option>';
                        }
                    ?>
                    </select>
                <div class="datetime" style="margin-top:20px">
                    <span style="background-color:rgb(41, 218, 47);
                    display: inline-block;
                    padding : 10px 10px;
                    color: white;
                    width: 120px;
                    border:2px solid rgb(26, 117, 255)"for = "date form-control">Ngày Sinh <span style="color:red;">*</span></span>
                    <input class="date" type="text" name="date" placeholder="dd/mm/yyyy" style=" margin-left: 6%;
                    border:2px solid rgb(26, 117, 255);
                    height: 42px;
                    width: 150px"
                    />
                </div>
                <div class="address" style="margin-top : 20px; margin-bottom : 20px">
                    <span style="background-color:rgb(41, 218, 47);
                    display: inline-block;
                    padding : 10px 10px;
                    color :white;
                    width: 120px;
                    border:2px solid rgb(26, 117, 255)" for="address">Địa chỉ</span>
                    <input type="text" name="address" style=" margin-left: 6%;
                    border:2px solid rgb(26, 117, 255);
                    height: 40px;
                    width: 370px"
                    />
                    
                </div>
                </div>
                    <div class="sign_up_button">
                        <input type="submit" name = "submit" value = "Đăng ký" style="background-color:rgb(41, 218, 47);
                        color :white;
                        padding :15px 40px;
                        margin-left: 23%; margin-top: 10px;
                        border:2px solid rgb(26, 117, 255);
                        border-radius:15px;">
                    </div>
            </div>
    </form>
</div>
</body>
</html>
<script type="text/javascript">  
                        $('.date').datepicker({  
                        format: 'dd-mm-yyyy'  
                        });  
                    </script>  
<style>
    .subject{
        margin-top:30px;
    };
    
</style>
